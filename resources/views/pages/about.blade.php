@extends("default")

@section('title', $title) 

@section('content')

    <h1> {{ $title }}</h1>
    <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid omnis reiciendis earum harum facilis blanditiis, voluptas mollitia error alias laudantium ut facere! Quasi, consequatur laudantium ad cupiditate laborum accusamus. Necessitatibus?</p>

    <ul>
        @forelse($numbers as $number)
            <li> {{ $number }}</li>
        @empty
            <li> Aucun chiffre </li>
       
        @endforelse
    </ul>

@endsection

@section('sidebar')

    <h3> A propos </h3>
    <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit accusantium, consectetur, qui eaque illum doloremque magnam laboriosam hic ipsam eligendi optio nulla expedita modi quibusdam mollitia molestiae nisi perspiciatis nihil? </p>
@endsection