<?php

namespace App\Http\Controllers;

use Request;
use App\Link;
use \Input;
use App\Http\Controllers\Controller;
use App\Http\Requests;


class LinksController extends Controller
{
    public function show($id){

        $link = Link::findOrFail($id);
        return redirect($link->url, 301);
    }
    
    public function create(){

        return view('links.create');
    }

    public function store(){

        $url = Request::get('url');
        $link = Link::firstOrCreate(['url' => $url]);

        
        return view ('links.success', compact('link'));
    }
}
