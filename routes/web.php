<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', function(\App\CacheInterface $app){

 //  dd(Cache2::get('test'));
//});


Route::get('/', function () {
   return view('welcome');
}); 

Route::get('a-propos', ['as' => 'about', 'uses' => 'PagesController@about']);

Route::get('links/create', 'LinksController@create');

Route::post('links/store', 'LinksController@store')->name("links:store");

Route::get('r/{id}', 'LinksController@show')->where('id', '[0-9]+');

Route::resource('news', 'PostsController');

Route::group(['middleware'=> ['web']], function(){

   Auth::routes(); Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function(){
       Route::resource('posts', 'PostsController'); }); });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
