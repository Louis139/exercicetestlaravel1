<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests;
use App\Category;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\EditPostRequest;
use Illuminate\Support\Facades\Session;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::with('category')->get();
        return view ('posts.index', compact('posts'));
    }

    public function create()
    {
        $post = new Post();
        $categories = Category::pluck('name', 'id');
        return view ('posts.create', compact('post', 'categories'));
    }

    public function store(Request $request)
    {
        $post = Post::create($request->all());
        $categories = Category::pluck('name', 'id');
        return redirect(route('news.edit', $post));
    }

    public function show($id)
    {
        $post = Post::published()->where('id', $id)->firstOrFail();
        return $post;
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $categories = Category::pluck('name', 'id');
        return view('posts.edit', compact('post', 'categories'));
    }

    public function update($id, EditPostRequest $request)
    {
            $post = Post::findOrFail($id);
            $post->update($request->all());
            return redirect(route('news.edit', $id))->with('success', 'L\'article a bien été sauvegardé');
    }

    public function destroy($id)
    {

    }
}
